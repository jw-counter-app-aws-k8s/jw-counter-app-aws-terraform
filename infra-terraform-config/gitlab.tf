resource "aws_iam_user" "jw_counter_gitlab_deploy_user" {
  name = "jw_counter_gitlab_deploy_user"
  path = "/"
}

resource "aws_iam_user_policy" "jw_counter_gitlab_deploy_policy" {
  name = "jw_counter_gitlab_deploy_policy"
  user = aws_iam_user.jw_counter_gitlab_deploy_user.name

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Effect   = "Allow"
        Action   = ["s3:ListBucket"]
        Resource = ["${aws_s3_bucket.jw_counter_bucket.arn}"]
      },
      {
        Effect   = "Allow"
        Action   = "s3:*Object"
        Resource = ["${aws_s3_bucket.jw_counter_bucket.arn}/*"]
      },
      {
        Effect   = "Allow"
        Action   = "autoscaling:StartInstanceRefresh"
        Resource = "*"
      }
    ]
  })
}
