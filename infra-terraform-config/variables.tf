variable "region" {
  type        = string
  description = "Region of resources"
  default     = "ap-southeast-1"
}

variable "availability_zone_1" {
  type        = string
  description = "AZ of resources"
  default     = "ap-southeast-1a"
}

variable "availability_zone_2" {
  type        = string
  description = "AZ of resources"
  default     = "ap-southeast-1b"
}
