# Create private S3 bucket and upload static code
resource "aws_s3_bucket" "jw_counter_bucket" {
  bucket = "jw-counter-bucket"
}

resource "aws_s3_object" "s3_counter_app_html" {
  bucket = aws_s3_bucket.jw_counter_bucket.id
  key    = "counter-app-code/counter-app.html"
  source = "${path.module}/../counter-app-code/counter-app.html"
  etag = filemd5("${path.module}/../counter-app-code/counter-app.html")
}

resource "aws_s3_object" "s3_counter_app_css" {
  bucket = aws_s3_bucket.jw_counter_bucket.id
  key    = "counter-app-code/counter-app.css"
  source = "${path.module}/../counter-app-code/counter-app.css"
  etag = filemd5("${path.module}/../counter-app-code/counter-app.css")
}

resource "aws_s3_object" "s3_counter_app_js" {
  bucket = aws_s3_bucket.jw_counter_bucket.id
  key    = "counter-app-code/counter-app.js"
  source = "${path.module}/../counter-app-code/counter-app.js"
  etag = filemd5("${path.module}/../counter-app-code/counter-app.js")
}

# Create IAM role & instance profile for EC2 instance to get bucket objects

data "aws_iam_policy_document" "instance_assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "counter_bucket_get_obj_role" {
  name               = "counter_bucket_get_obj_role"
  path               = "/"
  assume_role_policy = data.aws_iam_policy_document.instance_assume_role_policy.json
}

resource "aws_iam_role_policy" "counter_bucket_get_obj_policy" {
  name = "counter_bucket_get_obj_policy"
  role = aws_iam_role.counter_bucket_get_obj_role.id

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Effect   = "Allow"
        Action   = ["s3:ListBucket"]
        Resource = ["${aws_s3_bucket.jw_counter_bucket.arn}"]
      },
      {
        Effect   = "Allow"
        Action   = [
          "s3:PutObject",
          "s3:GetObject",
          "s3:DeleteObject"
        ]
        Resource = ["${aws_s3_bucket.jw_counter_bucket.arn}/*"]
      }
    ]
  })
}

resource "aws_iam_instance_profile" "counter_bucket_get_obj_profile" {
  name = "counter_bucket_get_obj_profile"
  role = aws_iam_role.counter_bucket_get_obj_role.name
}
