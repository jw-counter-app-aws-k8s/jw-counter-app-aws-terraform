resource "aws_security_group" "sg_server_fleet_a" {
  name        = "secgroup-server-fleet-a"
  description = "Server Fleet A instances Security Group"
  vpc_id      = data.aws_vpc.jw_default_vpc.id

  ingress {
    description = "HTTP from default VPC"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = [data.aws_vpc.jw_default_vpc.cidr_block]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

data "aws_ami" "ami_amazon_linux_2" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*-x86_64-gp2"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

resource "aws_launch_template" "template_server_fleet_a" {
  name                   = "template-server-fleet-a"
  image_id               = data.aws_ami.ami_amazon_linux_2.id
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.sg_server_fleet_a.id]
  update_default_version = true

  user_data = base64encode(templatefile("${path.module}/scripts/startup.tftpl", { NGINX_CONF = file("${path.module}/scripts/nginx.conf") }))

  iam_instance_profile {
    name = aws_iam_instance_profile.counter_bucket_get_obj_profile.name
  }

  monitoring {
    enabled = true
  }

  depends_on = [aws_iam_instance_profile.counter_bucket_get_obj_profile]
}

resource "aws_autoscaling_group" "server_fleet_a" {
  name                    = "server-fleet-a"
  vpc_zone_identifier     = [aws_subnet.jw_counter_private_snet.id]
  desired_capacity        = 3
  max_size                = 6
  min_size                = 3
  default_instance_warmup = 60

  launch_template {
    id      = aws_launch_template.template_server_fleet_a.id
    version = aws_launch_template.template_server_fleet_a.latest_version
  }

  instance_refresh {
    strategy = "Rolling"
  }

  depends_on = [module.alb]
}

resource "aws_autoscaling_attachment" "server_fleet_a_alb_attach" {
  autoscaling_group_name = aws_autoscaling_group.server_fleet_a.name
  lb_target_group_arn    = module.alb.target_group_arns[0]
}

resource "aws_autoscaling_policy" "example" {
  autoscaling_group_name = aws_autoscaling_group.server_fleet_a.name
  name                   = "server-fleet-a-scale-policy"
  policy_type            = "TargetTrackingScaling"

  estimated_instance_warmup = 300
  target_tracking_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ASGAverageCPUUtilization"
    }
    target_value = "70"
  }
}
