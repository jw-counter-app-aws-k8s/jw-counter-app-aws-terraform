# Get default VPC and a default subnet

data "aws_vpc" "jw_default_vpc" {
  default = true
}

data "aws_subnet" "jw_default_public_snet_1" {
  vpc_id            = data.aws_vpc.jw_default_vpc.id
  availability_zone = var.availability_zone_1
  default_for_az    = true
}

data "aws_subnet" "jw_default_public_snet_2" {
  vpc_id            = data.aws_vpc.jw_default_vpc.id
  availability_zone = var.availability_zone_2
  default_for_az    = true
}

# Set up private subnet with NAT gateway in default VPC

resource "aws_subnet" "jw_counter_private_snet" {
  vpc_id                  = data.aws_vpc.jw_default_vpc.id
  cidr_block              = cidrsubnet(data.aws_vpc.jw_default_vpc.cidr_block, 4, 15)
  availability_zone       = var.availability_zone_1
  map_public_ip_on_launch = false
}

resource "aws_eip" "jw_counter_nat_eip" {
  domain = "vpc"
}

resource "aws_nat_gateway" "jw_counter_nat" {
  allocation_id = aws_eip.jw_counter_nat_eip.id
  subnet_id     = data.aws_subnet.jw_default_public_snet_1.id
  depends_on = [
    aws_eip.jw_counter_nat_eip,
    aws_subnet.jw_counter_private_snet
  ]
}

resource "aws_route_table" "jw_counter_route_table" {
  vpc_id = data.aws_vpc.jw_default_vpc.id

  # Default VPC internal routing
  route {
    cidr_block = data.aws_vpc.jw_default_vpc.cidr_block
    gateway_id = "local"
  }

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.jw_counter_nat.id
  }
}

resource "aws_route_table_association" "jw_counter_route_table_assoc" {
  subnet_id      = aws_subnet.jw_counter_private_snet.id
  route_table_id = aws_route_table.jw_counter_route_table.id
}
