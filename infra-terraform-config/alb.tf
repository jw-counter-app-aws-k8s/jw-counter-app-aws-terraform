resource "aws_security_group" "sg_alb_server_fleet_a" {
  name        = "secgroup-alb-server-fleet-a"
  description = "Security Group for Server Fleet A ALB"
  vpc_id      = data.aws_vpc.jw_default_vpc.id

  ingress {
    description = "HTTP from clients"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    description      = "HTTP to Server Fleet A instances security group"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    security_groups  = [aws_security_group.sg_server_fleet_a.id]
  }
}

module "alb" {
  source  = "terraform-aws-modules/alb/aws"
  version = "~> 8.0"

  name = "alb-server-fleet-a"

  load_balancer_type = "application"

  vpc_id          = data.aws_vpc.jw_default_vpc.id
  subnets         = [data.aws_subnet.jw_default_public_snet_1.id, data.aws_subnet.jw_default_public_snet_2.id]
  security_groups = [aws_security_group.sg_alb_server_fleet_a.id]

  # Empty target group to attach ASG to. Default HTTP health checks for 200 OK
  target_groups = [
    {
      name             = "server-fleet-a-target-group"
      backend_protocol = "HTTP"
      backend_port     = 80
      target_type      = "instance"
    }
  ]

  http_tcp_listeners = [
    {
      port               = 80
      protocol           = "HTTP"
      target_group_index = 0
    }
  ]

  depends_on = [aws_security_group.sg_alb_server_fleet_a]
}
