# Counter App in AWS with Terraform

## Purpose
1. Create an Auto Scaling Group of EC2 instances that serve a website via Nginx, using content pulled from a private S3 bucket. EC2 instances placed behind an Application Load Balancer which is the only public internet-facing component.
2. Run a GitLab pipeline when changes in *counter-app-code/* (web content source code directory) are detected on master. Pipeline pushes new web content to S3 bucket and triggers instance refresh of ASG for instances to pull updated content from the bucket.
    - If failed by an instance refresh in progress, pipeline will wait then retry launching instance refresh.

## Screenshots
1. [Website original](images/1.%20Website%20Original.png)
2. [Website text in uppercase, after updating source code to trigger GitLab Pipeline deploy](images/2.%20Website%20Uppercase.png)

## Instructions

### [Architecture Diagram](images/Project%20Architecture%20Diagram.png)

### AWS Infrastructure Setup

1. Configure authentication to AWS for the Terraform AWS provider.
https://registry.terraform.io/providers/hashicorp/aws/latest/docs#authentication-and-configuration
2. From root directory, run `cd infra-terraform-config`.
3. Run `terraform init`, then `terraform apply`.
4. The following AWS resources are created or used in the `ap-southeast-1` (Singapore) region:
    1. A **private S3 bucket** and an **instance profile** with attached IAM role and policies for EC2 instances to access bucket objects. Web content source code uploaded to bucket.
    2. **Default VPC**, **default VPC public subnets**, a **private subnet**, a **NAT gateway** with an **EIP**, a **route table** for private subnet's internal VPC and NAT gateway traffic.
    3. An **Auto Scaling Group** (Server Fleet A) of at least 3 instances based on Amazon Linux 2 launch template with instance profile from point 1 attached; pulls web content from S3 and runs Nginx to serve it. An **Autoscaling policy** that scales by average CPU utilization in the group.
    4. An **Application Load Balancer** with port 80 HTTP listeners and the ASG from point 3 attached as target group.
    5. **Security Group for ASG** that only allows inbound port 80 within default VPC and outbound to reach install servers and S3 buckets. **Security Group for ALB** that only allows port 80 from clients and port 80 to Security Group for ASG.
    6. An **IAM user for GitLab pipeline** with S3 object and ASG start instance refresh permissions.
5. Get the public DNS name of ALB from console or CLI. Enter it on browser **with `http://`** to see the Increment and Decrement Counter page.

### GitLab Pipeline Setup

1. Create and save the access key for GitLab IAM user `jw_counter_gitlab_deploy_user` from console or CLI to use in the next step.
2. In this repo, add the following masked and protected variables at `Settings -> CI/CD -> Variables`: `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`, `AWS_DEFAULT_REGION=ap-southeast-1`.
    - A more secure way is to use [OpenID with AWS IAM to get credentials](https://docs.gitlab.com/ee/ci/cloud_services/aws/). Pipeline vars are used for now so that this repo and pipeline can be easily copied and simply used with own vars for testing by others.
3. Pipeline triggers on push or merge to master, not on branch or merge request. Only runs when there are changes to *counter-app-code/* directory.
    - If instance refresh stage is failed by an in-progress instance refresh, pipeline sleeps then retries.

### Destroy AWS Cluster

1. Access keys created manually for GitLab IAM user must be deleted before next step.
2. Run `terraform destroy`.

## Improvements
1. Enforce HTTPS instead of HTTP connections from clients. Have HTTP to HTTPS redirection on ALB.
2. Use containerized app deployments on long-lived instances instead of recreating instances on every update. Re-provisioning instances for updates is slow & new attempts may be blocked by previous ASG instance refresh.
3. For ASG, find optimal settings to complete instance refreshes faster. E.g. optimal instance warmup time after instance is ready so that ASG moves on to replacing the next instance as soon as possible. Or decrease min healthy percentage so more instances are replaced simultaneously; balance this with high availability requirement.
4. Add tags to resources for grouping, billing, monitoring, environment division etc.
5. Be as strict as possible with IAM, only allow the exact permissions required. 
6. Set optimal Autoscaling policies for scaling of ASG, e.g. number of ALB requests, suitable CPU utilization thresholds etc.
7. Have a point of entry like bastion or jumpbox for infra team to maintain and troubleshoot private EC2 instances.
8. Use OpenID Connect for GitLab CI/CD to use AWS IAM for authentication, as mentioned [in point 2 of GitLab Pipeline Setup section](#gitlab-pipeline-setup).
